﻿using EmailGetterV1.Enums;
using EmailGetterV1.Helpers;
using EmailGetterV1.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailGetterV1.Controllers
{
    public class BaseController : Controller
    {
        protected JsonResult ResponseSuccess<T>(T responseObject, Pagination pagination = null)
        {
            return Json(new Response()
            {
                status = ResponseStatus.Success,
                //Message = "Operation completed successfully!",
                succsess = true,
                Data = responseObject,
                pagination = pagination ?? null
      
            });
        }

        protected JsonResult ResponseFailed(string message)
        {
            return Json(new Response()
            {
                status = ResponseStatus.Failed,
                Message = message,
                //Message = "Sorry, something went wrong. Please try again later.",
                succsess = false,
                Data = null
            });
        }
    }
}
