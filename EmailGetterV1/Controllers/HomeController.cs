﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmailGetterV1.Models;
using EmailGetterV1.Service;
using MailKit.Net.Imap;
using MimeKit;
using MailKit.Security;
using MailKit;
using MailKit.Search;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Gmail.v1;
using Google.Apis.Auth.OAuth2.Flows;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.IO;
using Google.Apis.Services;
using Google.Apis.Gmail.v1.Data;
using MailKit.Net.Smtp;
using Google.Apis.Auth.OAuth2.Responses;

namespace EmailGetterV1.Controllers
{

    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        private static readonly IAuthorizationCodeFlow flow =
            new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
            {
                ClientSecrets = new ClientSecrets
                {
                    ClientId = "909769669933-7he8hli3m3n3mnda5e02p1b4vqnot774.apps.googleusercontent.com",
                    ClientSecret = "ZljKJCMFX9ALeoddd9RkKhEc"
                },
                Scopes = new[] { GmailService.Scope.MailGoogleCom },
                
                DataStore = new FileDataStore("Drive.Api.Auth.Store")
            });
        [Route("Home/GetMessages")]
        public JsonResult GetMessages([FromBody]dynamic value)
        {
            try
            {
                string username = value["username"];
                string messageBox = value["messageBoxName"];
                int currentPage = value["page"];
                var emailservice = new EmailService();
                int totalmessages = 0;
                var message = emailservice.GetMessages(username, messageBox, currentPage, out totalmessages);
                var paging = new Pagination()
                {
                    page = currentPage,
                    totalItems = totalmessages
                };

                return ResponseSuccess(message, paging);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }
         
        }
        public JsonResult GetMessagesById([FromBody]dynamic value)
        {
            try
            {
                string username = value["username"];
                string messageBox = value["messageBoxName"];
                int currentPage = value["page"];
                var referencedMessageIds = value["referencedMessageIds"];
                var emailservice = new EmailService();
                int totalmessages = 0;
                var message = emailservice.GetMessagesById(username, messageBox, currentPage, referencedMessageIds, out totalmessages);
                var paging = new Pagination()
                {
                    page = currentPage,
                    totalItems = totalmessages
                };

                return ResponseSuccess(message, paging);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }

        }
        [Route("gmail/folders")]
        public async Task<JsonResult> GetFoldersWithMessageCount()
        {
            try
            {
                List<string> folderName = new List<string>();
                folderName.Add("INBOX");
                var emailservice = new EmailService();
                var folders = await emailservice.GetFoldersWithMessageCount(folderName);

                return ResponseSuccess(folders);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }

        }

        [Route("gmail/threads")]
        public JsonResult GetMessagesByFolder(string folder, int page, int page_size)
        {
            try
            {
                int totalItems = 0;
                var emailservice = new EmailService();
                var messages = emailservice.GetMessagesByFolder(folder, page, page_size, out totalItems);

                var paging = new Pagination()
                {
                    page = page,
                    pageSize = page_size,
                    totalItems = totalItems
                };

                return ResponseSuccess(messages, paging);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }

        }
        [Route("gmail/attachment/{messageId}")]
        public JsonResult GetAttachment(string messageId, string fileName)
        {
            try
            {

                var emailservice = new EmailService();
                var messages = emailservice.GetAttachment(messageId, fileName);

                return ResponseSuccess(messages);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }

        }

        [Route("gmail/message/{threadId}")]
        public JsonResult GetMessage(ulong threadId)
        {
            try
            {
                var emailservice = new EmailService();
                var message = emailservice.GetMessageById(threadId);

                return ResponseSuccess(message);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }

        }
        [Route("gmail/addFlag")]
        public JsonResult GmailMessagesAddflag(string folderName, string flag, string uniqueIds)
        {
            try
            {
                var uniqueIdList = uniqueIds?.Split(",");
                var uIds = uniqueIdList?.Select(s => MailKit.UniqueId.Parse(s)).ToList();
                var emailservice = new EmailService();
                emailservice.GmailMessagesAddflag(folderName, flag, uIds);

                return ResponseSuccess(string.Empty);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }

        }

        [Route("gmail/removeFlag")]
        public JsonResult GmailMessagesRemoveflag(string folderName, string flag, string uniqueIds)
        {
            try
            {
                var uniqueIdList = uniqueIds?.Split(",");
                var uIds = uniqueIdList?.Select(s => MailKit.UniqueId.Parse(s)).ToList();
                var emailservice = new EmailService();
                emailservice.GmailMessagesRemoveflag(folderName, flag, uIds);

                return ResponseSuccess(string.Empty);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }

        }

        [Route("gmail/messages")]
        public JsonResult SearchThreads(string folder,string search, int page, int page_size)
        {
            try
            {
                var totalItems = 0;
                var emailservice = new EmailService();
                var threads = emailservice.SearchThreads(folder, search, page, page_size, out totalItems);
                var paging = new Pagination()
                {
                    page = page,
                    pageSize = page_size,
                    totalItems = totalItems
                };

                return ResponseSuccess(threads, paging);
            }
            catch (Exception ex)
            {
                return ResponseFailed(ex.Message);
            }
        }
       
        [Route("gmail/GooglePublicApi")]
        public JsonResult GooglePublicApi()
        {
            string[] Scopes = { GmailService.Scope.GmailReadonly };
            string ApplicationName = "Gmail API .NET Quickstart";


            UserCredential credential;
            using (var stream =
        new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                //Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            List<Google.Apis.Gmail.v1.Data.Thread> result = new List<Google.Apis.Gmail.v1.Data.Thread>();
            UsersResource.ThreadsResource.ListRequest request = service.Users.Threads.List("me");

            do
            {
                try
                {
                    ListThreadsResponse response = request.Execute();
                    result.AddRange(response.Threads);
                    request.PageToken = response.NextPageToken;
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                }
            } while (!String.IsNullOrEmpty(request.PageToken));
            // Define parameters of request.
            //UsersResource.LabelsResource.ListRequest request = service.Users.Labels.List("me");
            UsersResource.ThreadsResource.ListRequest vajs = service.Users.Threads.List("me");
            //List<Google.Apis.Gmail.v1.Data.Thread> result = new List<Google.Apis.Gmail.v1.Data.Thread>();
            List<Google.Apis.Gmail.v1.Data.Thread> result1 = new List<Google.Apis.Gmail.v1.Data.Thread>();
            var aaa = service.Users.Messages.List("me");

            //var requestw = service.Users.Messages.List(result.u);
            var aaaaaa = vajs.Execute();
            var www = aaaaaa.Threads;

            result.AddRange(www);
            foreach (var item in result)
            {
                var msg = service.Users.Threads.Get("testgml922@gmail.com", item.Id).Execute();
                result1.Add(msg);
            }

            return Json("");
        }
    }
}
