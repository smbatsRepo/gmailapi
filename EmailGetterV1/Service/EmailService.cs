﻿using EAGetMail;
using EmailGetterV1.Communicator;
using EmailGetterV1.Models;
using Google.Apis.Gmail.v1;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit.Security;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace EmailGetterV1.Service
{
    public class EmailService
    {
        //var query = SearchQuery.SubjectContains("MimeKit").Or(SearchQuery.SubjectContains("MailKit"));
        //var uids = client.Inbox.Search(query);
        //public string token = "ya29.a0Adw1xeU9sA9q21An51VQAt0qrhnlLrj5wD0X5jpdiNUiEBWQw531ySavwbhTLngm_V_zQ7R0Gmw-JdymXLOS3Pqkaw-3_b9tBHACGClrPNPo4CwDMx0jujtdbPkyVZM5xBys-CrGX_eN-yanHpXrsaTDDkTIY0-Wyvc";
        public string token = "ya29.a0Adw1xeX1MFjwKnjEzXue7TMhgsDABT5OCbUnBYeQQXuHDulGHu7R_mLUSQPKfW2XGqGU9IdV_Cmf0RqvZKSgTFeXYwgiqje38cznr_oAyVr-_LppcjuBW4tLQKzsBV47cY_-kstbxcuUfVpWtg_j6-8wfWDu9GGZWOY";
        //public string token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImNiNDA0MzgzODQ0YjQ2MzEyNzY5YmI5MjllY2VjNTdkMGFkOGUzYmIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNTM4MTU4ODUwNDY1LWpkcTAwcW4xNnQzNnFxNGxldDQ2aWY4bmFidWwwMjliLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNTM4MTU4ODUwNDY1LWpkcTAwcW4xNnQzNnFxNGxldDQ2aWY4bmFidWwwMjliLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAxMTkzODE1MzI1NjQ1MTA0Mzg0IiwiZW1haWwiOiJnb3JuYXphcnlhbjkxQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiWjAxZzY1RzIzS2xxTVBCX1EyZ0VBdyIsIm5hbWUiOiJHb3IgTmF6YXJ5YW4iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EtL0FPaDE0R2hqLUk2VHJkWDNWSlpxSFBQM1BNcl9yYXRLVHpVVEZxRTdYN0hKRFE9czk2LWMiLCJnaXZlbl9uYW1lIjoiR29yIiwiZmFtaWx5X25hbWUiOiJOYXphcnlhbiIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNTg0MTIxNzc1LCJleHAiOjE1ODQxMjUzNzUsImp0aSI6ImNkMGI1MWJkMWVjOWI3MWE5OWI1YjU2NDM5YTEwNTVlOGVmZDBlODIifQ.MPHTAm-U6d4IUBD5kHmSV5dLrICZylTY5nU22WKuSGRZAepo2ZAMUmnc0xU-Uqy9rT_xLLsXowqQQiLJYMDY0IBRpLCq7VCHR9YFhiQbens3LJsUc-jxnMCBpaLG6HDrqfZf0fBYeVi9nnfXUShw95GHlSQ_0V4JDe-xaFQjmFo9Q9zQRcbxW5uOrtH6awYMz8VRAKu0rbgQUakJEXQaP_NWi3mi4j5EkeRftVo7UIW7jW6yjrMBxFGBkMfC5IuyfFdPr9oRFuF_A13jRwOI5yhfbdNwSeteowVZbMOzfrUqkpCIzjLxSCp5cqk09KXG3zaPcHAT8GQMyunz0vP9lg";
        public string Password = "Atestg123";
        public string username = "testgml922@gmail.com";
        //public string Password = "SMBAT001!GMAIL";
        //public string username = "smbatchilingaryan28@gmail.com";
        public List<GmailThread> GetMessages(string username, string messageBoxName, int page, out int totalmessages)
        {
            List<GmailThread> messages = new List<GmailThread>();
            using (var imap = new ImapClient())
            {
                
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                //var oauth2 = new SaslMechanismOAuth2(username, Password);

                imap.Authenticate(username, Password);
                imap.GetFolder(messageBoxName).Open(FolderAccess.ReadOnly);
                var messagesth = imap.Inbox.Fetch(0, -1, MessageSummaryItems.UniqueId |
                                                    MessageSummaryItems.Envelope | MessageSummaryItems.References);

                var threads = MessageThreader.Thread(messagesth, ThreadingAlgorithm.References);
                
                totalmessages = imap.GetFolder(messageBoxName).Count;

                var messagesT = imap.GetFolder(messageBoxName).Fetch(0,-1, MessageSummaryItems.All |
                    MessageSummaryItems.References |
                    MessageSummaryItems.GMailLabels |
                    MessageSummaryItems.PreviewText |
                    MessageSummaryItems.ModSeq);

                foreach (var info in messagesT)
                {
                    var message = new GmailThread()
                    {
                        threadID = info.GMailThreadId.Value,
                        subject = info.Envelope.Subject,
                        date = info.Envelope.Date.Value,
                        hasAttachment = info.Attachments.Any(),
                        to = info.Envelope.To.Mailboxes.Select(a => new Receiver { address = a.Address, name = a.Name }).ToList(),
                        from = info.Envelope.From.Mailboxes.Select(a => new Sender { address = a.Address, name = a.Name }).ToList(),
                        flag = info.Flags.ToString(),
                        //referencedMessageIds = info.References,
                        //fields = info.Fields,
                        //isReply = info.IsReply,
                        //labels = info.GMailLabels,
                        snippet = info.PreviewText

                    };

                    messages.Add(message);
                }
                imap.Disconnect(true);
            }

            return messages;

        }
        public List<GmailThread> GetMessagesById(string username, string messageBoxName, int page, string referencedMessageIds, out int totalmessages)
        {
            List<GmailThread> messages = new List<GmailThread>();
            using (var imap = new ImapClient())
            {
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                var oauth2 = new SaslMechanismOAuth2(username, token);
                imap.Authenticate(oauth2);
                imap.GetFolder(messageBoxName).Open(FolderAccess.ReadOnly);
                totalmessages = imap.GetFolder(messageBoxName).Count;
                //var query = SearchQuery.GMailMessageId((ulong) referencedMessageIds );
                //var uids = imap.Inbox.Search(query);

                //var messagesT = imap.GetFolder(messageBoxName).Fetch(uids, MessageSummaryItems.All |
                //    MessageSummaryItems.References |
                //    MessageSummaryItems.GMailLabels |
                //    MessageSummaryItems.PreviewText |
                //    MessageSummaryItems.ModSeq);

                //foreach (var info in messagesT)
                //{
                //    var message = new Message()
                //    {
                //        messageID = info.Envelope.MessageId,
                //        subject = info.Envelope.Subject,
                //        date = info.Envelope.Date,
                //        hasAttachment = info.Attachments.Any(),
                //        receivers = info.Envelope.To.Mailboxes.Select(a => new Receiver { address = a.Address, name = a.Name }).ToList(),
                //        senders = info.Envelope.From.Mailboxes.Select(a => new Sender { address = a.Address, name = a.Name }).ToList(),
                //        flag = info.Flags.ToString(),
                //        referencedMessageIds = info.References,
                //        fields = info.Fields,
                //        isReply = info.IsReply,
                //        labels = info.GMailLabels,
                //        snippet = info.PreviewText.Replace(Environment.NewLine, "")

                //    };

                //    messages.Add(message);
                //}
                imap.Disconnect(true);
            }

            return messages;

        }
        public async Task<List<Folder>> GetFoldersWithMessageCount(List<string> folderNames)
        {
            List<Folder> folders = new List<Folder>();
            using (var imap = new ImapClient())
            {
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                //var oauth2 = new SaslMechanismOAuth2(username, Password);
                imap.Authenticate(username, Password);
                //var gmailfolders = imap.GetFolder(imap.PersonalNamespaces[0]);
                //var folderList = CollectAllFolders(gmailfolders, new List<IMailFolder>());
                foreach (var folder in folderNames)
                {
                    var currentFolder = await imap.GetFolderAsync(folder);
                    await currentFolder.OpenAsync(FolderAccess.ReadOnly);

                    var allUnreadMessageIds = await currentFolder.SearchAsync(SearchQuery.NotSeen);

                    var messages = await currentFolder.FetchAsync(0, -1, //MessageSummaryItems.All |
                                                                         //MessageSummaryItems.References |
                                                                         MessageSummaryItems.UniqueId |
                                                                         MessageSummaryItems.GMailThreadId);

                    var allUnreadMessages = await currentFolder.FetchAsync(allUnreadMessageIds, //MessageSummaryItems.All |
                                                                                                //MessageSummaryItems.References |
                                                                                                MessageSummaryItems.UniqueId |
                                                                                                MessageSummaryItems.GMailThreadId);
                    var messagesList = messages.ToList();
                    var unreadMessagesList = allUnreadMessages.ToList();

                    var gmailThreads = messages.GroupBy(g => g.GMailThreadId).Count();
                    var gmailUnreadThreads = allUnreadMessages.GroupBy(g => g.GMailThreadId).Count();

                    //var threads = MessageThreader.Thread(messagesList, ThreadingAlgorithm.References | ThreadingAlgorithm.OrderedSubject).Count;
                    //var threadsUnseen = MessageThreader.Thread(unreadMessagesList, ThreadingAlgorithm.References | ThreadingAlgorithm.OrderedSubject).Count;

                    var folderInfo = new Folder()
                    {
                        name = currentFolder.Name,
                        messagesTotal = messagesList.Count(),
                        messagesUnread = unreadMessagesList.Count(),
                        threadsTotal = gmailThreads,
                        threadsUnread = gmailUnreadThreads
                    };
                    folders.Add(folderInfo);
                } 
                imap.Disconnect(true);
            }

            return folders;
        }
        public List<GmailThread> GetMessagesByFolder(string folder, int page, int pageSize, out int totalItems)
        {


            List<GmailThread> messages = new List<GmailThread>();
            //IList<UniqueId> uniqueIds = new List<UniqueId>();
            Dictionary<UniqueId, int> uniqueIds = new Dictionary<UniqueId, int>();
            var paging = new Paging(page, pageSize);
            using (var imap = new ImapClient())
            {
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                //var oauth2 = new SaslMechanismOAuth2(username, Password);
                imap.Authenticate(username, Password);
                var gmailFolder = imap.GetFolder(folder);
                gmailFolder.Open(FolderAccess.ReadOnly);


                var gmailMessages = gmailFolder.Fetch(0, -1, //MessageSummaryItems.All |
                                                             //MessageSummaryItems.PreviewText | 
                                                             MessageSummaryItems.UniqueId |
                                                             MessageSummaryItems.GMailMessageId |
                                                             MessageSummaryItems.GMailThreadId)
                                                             .OrderByDescending(o => o.UniqueId)
                                                             .ToList();
             

                var groupedList = gmailMessages.GroupBy(g => g.GMailThreadId)
                                               .Select(s => new { threadId = s.Key, referanceCount = s.Count(), messages = s.ToList() })
                                               .ToList();

                totalItems = groupedList.Count();
                var finalList = groupedList.Skip(paging.skip).Take(pageSize);

                foreach (var item in finalList)
                {
                    var uniqueId = item.messages.Where(w => w.GMailMessageId.Value == item.threadId.Value || w.GMailThreadId.Value == item.threadId.Value).FirstOrDefault().UniqueId;
                    var refcount = item.referanceCount;
                    uniqueIds.Add(uniqueId, refcount);
                }

                IList<UniqueId> newlist = uniqueIds.Select(s=>s.Key).ToList();
                var messageSummaryList = gmailFolder.Fetch(newlist,   MessageSummaryItems.All |
                                                                      MessageSummaryItems.References |
                                                                      MessageSummaryItems.PreviewText |
                                                                      MessageSummaryItems.UniqueId |
                                                                      MessageSummaryItems.GMailThreadId)
                                                                      .OrderByDescending(o => o.UniqueId)
                                                                      .ToList();


                //if we want to do 1 call to Gmail API, we can use "finalList" instead of "messageSummaryList"
                // and open all comments
                foreach (var info in messageSummaryList)
                {
                    //var gmailMessage = info.messages.Where(w=>w.GMailMessageId.Value == info.threadId.Value || w.GMailThreadId.Value == info.threadId.Value).FirstOrDefault();
                    var refCount = uniqueIds.Where(w => w.Key == info.UniqueId).Select(s=>s.Value).FirstOrDefault();
                    //string snippet = Regex.Replace(info.PreviewText, @"\t|\n|\r|--", "");
                   
                    var message = new GmailThread()
                    {
                        threadID = info.GMailThreadId.Value,
                        uniqueId = info.UniqueId.Id,
                        snippet = info.PreviewText,
                        subject = info.Envelope.Subject,
                        date = info.Envelope.Date,
                        //referencesCount = info.referanceCount <= 1 ? 0 : info.referanceCount,
                        referencesCount = refCount,
                        to = info.Envelope.To.Mailboxes.Select(a => new Receiver { address = a.Address, name = a.Name }).ToList(),
                        from = info.Envelope.From.Mailboxes.Select(a => new Sender { address = a.Address, name = a.Name }).ToList(),
                        flag = info.Flags.ToString(),
                        hasAttachment = info.Attachments.Any(),

                    };
                    messages.Add(message);
                }

                imap.Disconnect(true);
            }
            return messages;
        }
        public AttachmentInfo GetAttachment(string messageId, string fileName)
        {
            AttachmentInfo att = new AttachmentInfo();
            using (var imap = new ImapClient())
            {
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                //var oauth2 = new SaslMechanismOAuth2(username, Password);

                imap.Authenticate(username, Password);
                var gmailFolder = imap.GetFolder(SpecialFolder.All);
                gmailFolder.Open(FolderAccess.ReadOnly);
                var uid = gmailFolder.Search(SearchQuery.HeaderContains("Message-Id", messageId)).First();
                var gmailMessage = gmailFolder.GetMessage(uid);

                var attachment = gmailMessage.Attachments.Where(w => w.ContentDisposition.FileName == fileName).FirstOrDefault();
                if (attachment != null)
                {
                    var part = (MimePart)attachment;
                    //byte[] buffer = new byte[16 * 1024];
                    //using (MemoryStream ms = new MemoryStream())
                    // {
                    //int read;
                    //while ((read = part.Content.Stream.Read(buffer, 0, buffer.Length)) > 0)
                    //{
                    //    ms.Write(buffer, 0, read);
                    //}

                    //var ret = ms.ToArray();
                    using (var memory = new MemoryStream())
                    {
                        part.Content.DecodeTo(memory);
                        var buffer = memory.GetBuffer();
                        var length = (int)memory.Length;
                        var base64 = Convert.ToBase64String(buffer, 0, length);

                        att = new AttachmentInfo()
                        {
                            messageId = messageId,
                            fileName = fileName,
                            fileSize = part.Content.Stream.Length,
                            mediaSubtype = attachment.ContentType.MediaSubtype,
                            type = part.ContentType.MimeType,
                            content = base64,
                        };
                       //}
                    }
                }
                imap.Disconnect(true);
            }
            return att;
        }
        public GmailMessage GetMessageById(ulong threadId)
        {
            GmailMessage message = new GmailMessage();
            List<Message> messages = new List<Message>();
            List<Models.AttachmentInfo> AttachmentList = new List<Models.AttachmentInfo>();

            using (var imap = new ImapClient())
            {
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                //var oauth2 = new SaslMechanismOAuth2(username, Password);
                imap.Authenticate(username, Password);
                var gmailFolder = imap.GetFolder(SpecialFolder.All);
                gmailFolder.Open(FolderAccess.ReadOnly);
                var gmailThread = gmailFolder.Search(SearchQuery.GMailThreadId(threadId)).OrderByDescending(o => o.Id).ToList();

                var messageSummaryList = gmailFolder.Fetch(gmailThread, MessageSummaryItems.All |
                                                                                MessageSummaryItems.References |
                                                                                MessageSummaryItems.UniqueId |
                                                                                MessageSummaryItems.PreviewText |
                                                                                MessageSummaryItems.BodyStructure |
                                                                                MessageSummaryItems.Body |
                                                                                MessageSummaryItems.GMailMessageId |
                                                                                MessageSummaryItems.GMailThreadId)
                                                                                //.OrderByDescending(o=>o.UniqueId)
                                                                                .ToList();

                var mainMessage = messageSummaryList.Where(w => w.GMailMessageId.Value == threadId || w.GMailThreadId.Value == threadId).First();
                //var onlyForSnippet = gmailFolder.GetMessage(mainMessage.UniqueId);
                //var text = onlyForSnippet.TextBody.Substring(0, 300);
                //string removeNewLines= Regex.Replace(mainMessage.PreviewText, @"\t|\n|\r|--", "");
                //string removeUrls = Regex.Replace(removeNewLines, @"((http|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)", "");
                //string removeHtml = Regex.Replace(removeUrls, "<.*?>", "");
                string snippet = mainMessage.PreviewText;
    
                foreach (var item in messageSummaryList)
                {
                    var mimeMessage = gmailFolder.GetMessage(item.UniqueId);
                    
                    foreach (var attachment in mimeMessage.Attachments)
                    {
                        var part = (MimePart)attachment;

                        var attachmentInfo = new Models.AttachmentInfo()
                        {
                            messageId = item.Envelope.MessageId,
                            fileName = part.FileName,
                            mediaSubtype = attachment.ContentType.MediaSubtype,
                            type = attachment.ContentType.MimeType,
                            fileSize = part.Content.Stream.Length,
                        };

                        AttachmentList.Add(attachmentInfo);

                        //if (attachment is MessagePart)
                        //{
                        //    var fileName = attachment.ContentDisposition?.FileName;
                        //    var rfc822 = (MessagePart)attachment;

                        //    if (string.IsNullOrEmpty(fileName))
                        //        fileName = "attached-message.eml";

                        //    using (var stream = File.Create(fileName))
                        //        rfc822.Message.WriteTo(stream);
                        //}
                        //else
                        //{
                        //    // attachment to Base64
                        //    var part = (MimePart)attachment;
                        //    var fileName = part.FileName;
                        //    byte[] buffer = new byte[16 * 1024];
                        //    using (MemoryStream ms = new MemoryStream())
                        //    {
                        //        int read;
                        //        while ((read = part.Content.Stream.Read(buffer, 0, buffer.Length)) > 0)
                        //        {
                        //            ms.Write(buffer, 0, read);
                        //        }

                        //        var ret = ms.ToArray();
                        //        att.content = ret;
                        //        att.fileName = fileName;
                        //    }
                        //    attList.Add(att);
                        //}

                    }
                    //foreach (var mp in mimeMessage.Attachments)
                    //{
                    //    //try
                    //    //{
                    //    //    var attachment = (MimePart)mp;
                    //    //    mimePartStream.Add(attachment);
                    //    //    var attachment1 = new MimePart("image", "gif")
                    //    //    {
                    //    //        Content = new MimeContent(File.OpenRead(@"D:\TempFolder\tmp5B3A.pdf")),
                    //    //        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    //    //        ContentTransferEncoding = ContentEncoding.Base64,
                    //    //        FileName = Path.GetFileName(@"D:\TempFolder\tmp5B3A.pdf")
                    //    //    };

                    //    //    // now create the multipart/mixed container to hold the message text and the
                    //    //    // image attachment
                    //    //    var multipart = new Multipart("mixed");
                    //    //    //multipart.Add(body);
                    //    //    multipart.Add(attachment1);

                    //    //    // now set the multipart/mixed as the message body
                    //    //    mimeMessage.Body = multipart;
                    //    //    builder.Attachments.Add(attachment1);
                    //    //    mimeMessage.Body = builder.ToMessageBody();
                    //    //}
                    //    //catch (Exception ex)
                    //    //{

                    //    //}



                    //    //using (var stream = System.IO.File.Create(Path.GetTempFileName()))
                    //    //{
                    //    //    if (mp is MessagePart)
                    //    //    {
                    //    //        var part = (MessagePart)mp;

                    //    //        part.Message.WriteTo(stream);
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        var aaa = (MimePart)mp;
                    //    //        aaa.Content.Stream
                    //    //        mimePart.Content.DecodeTo(stream);
                    //    //    }
                    //    //}
                    //}

                    
                    var refMessage = new Message()
                    {
                        uniqueId = item.UniqueId.Id,
                        body = mimeMessage.HtmlBody?? mimeMessage.TextBody,
                        date = item.Envelope.Date.Value,
                        from = item.Envelope.From.Mailboxes.Select(a => new Sender { address = a.Address, name = a.Name }).ToList(),
                        to = item.Envelope.To.Mailboxes.Select(a => new Receiver { address = a.Address, name = a.Name }).ToList(),
                        Attachments = AttachmentList
                    };

                    messages.Add(refMessage);
                }

                message = new GmailMessage()
                {
                    threadID = threadId,
                    uniqueId = mainMessage.UniqueId.Id,
                    snippet = snippet,
                    subject = mainMessage.Envelope.Subject,
                    date = mainMessage.Envelope.Date.Value,
                    from = mainMessage.Envelope.From.Mailboxes.Select(a => new Sender { address = a.Address, name = a.Name }).ToList(),
                    to = mainMessage.Envelope.To.Mailboxes.Select(a => new Receiver { address = a.Address, name = a.Name }).ToList(),
                    flag = mainMessage.Flags.ToString(),
                    hasAttachment = mainMessage.Attachments.Any(),
                    references = messages,
                };

                imap.Disconnect(true);
            }

            return message;
        }
        public void GmailMessagesAddflag(string folderName, string flag, List<UniqueId> uniqueIds)
        {
            MailKit.MessageFlags messageFlags = new MailKit.MessageFlags();
            switch (flag)
            {
                case "read":
                    messageFlags = MailKit.MessageFlags.Seen;
                    break;
            }
            using (var imap = new ImapClient())
            {
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                //var oauth2 = new SaslMechanismOAuth2(username, Password);
                imap.Authenticate(username, Password);

                if(uniqueIds != null && uniqueIds.Any() && !string.IsNullOrEmpty(folderName))
                {
                    var gmailFolder = imap.GetFolder(folderName);
                    gmailFolder.Open(FolderAccess.ReadWrite);
                    gmailFolder.AddFlags(uniqueIds, messageFlags, true);
                }
                else
                {
                    var gmailFolder = imap.GetFolder(SpecialFolder.All);
                    gmailFolder.Open(FolderAccess.ReadWrite);
                    var msg = gmailFolder.Fetch(0, -1, MessageSummaryItems.Envelope | MessageSummaryItems.UniqueId);
                    var uIds = msg.Select(s => s.UniqueId).ToList();
                    gmailFolder.AddFlags(uIds, messageFlags, true);

                }

                imap.Disconnect(true);
            }
        }
        public void GmailMessagesRemoveflag(string folderName, string flag, List<UniqueId> uniqueIds)
        {
            MailKit.MessageFlags messageFlags = new MailKit.MessageFlags();
            switch (flag)
            {
                case "read":
                    messageFlags = MailKit.MessageFlags.Seen;
                    break;
            }
            using (var imap = new ImapClient())
            {
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                //var oauth2 = new SaslMechanismOAuth2(username, Password);
                imap.Authenticate(username, Password);

                if (uniqueIds != null && uniqueIds.Any() && !string.IsNullOrEmpty(folderName))
                {
                    var gmailFolder = imap.GetFolder(folderName);
                    gmailFolder.Open(FolderAccess.ReadWrite);
                    gmailFolder.RemoveFlags(uniqueIds, messageFlags, true);
                }
                else
                {
                    var gmailFolder = imap.GetFolder(SpecialFolder.All);
                    gmailFolder.Open(FolderAccess.ReadWrite);
                    var msg = gmailFolder.Fetch(0, -1, MessageSummaryItems.Envelope | MessageSummaryItems.UniqueId);
                    var uIds = msg.Select(s => s.UniqueId).ToList();
                    gmailFolder.RemoveFlags(uIds, messageFlags, true);

                }

                imap.Disconnect(true);
            }
        }
        public List<GmailThread> SearchThreads(string folder, string search, int page, int page_size, out int totalItems)
        {
            List<GmailThread> gmailThreads = new List<GmailThread>();
            Dictionary<IMessageSummary, int> msgList = new Dictionary<IMessageSummary, int>();
            var paging = new Paging(page, page_size);
            using (var imap = new ImapClient())
            {
                imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                //var oauth2 = new SaslMechanismOAuth2(username, Password);
                imap.Authenticate(username, Password);
                var gmailForder = imap.GetFolder(folder);
                gmailForder.Open(FolderAccess.ReadOnly);
                var query = SearchQuery.GMailRawSearch(search);
                var searchResult = gmailForder.Search(query);
                var messageSummaries = gmailForder.Fetch(searchResult, MessageSummaryItems.Envelope |
                                                                       MessageSummaryItems.GMailThreadId |
                                                                       MessageSummaryItems.GMailMessageId |
                                                                       MessageSummaryItems.PreviewText |
                                                                       MessageSummaryItems.UniqueId)
                                                                       //.OrderByDescending(o => o.Id)
                                                                       .ToList();

                var messageThreads = messageSummaries.GroupBy(g => g.GMailThreadId).Select(s => new { threadId = s.Key, messages = s.ToList(), referenceCount = s.Count() }).ToList();
                totalItems = messageThreads.Count();

                foreach (var item in messageThreads)
                {
                    var thread = item.messages.Where(w => w.GMailMessageId.Value == item.threadId.Value || w.GMailThreadId.Value == item.threadId.Value).Select(s=> new { thread = s, refcount = item.referenceCount }).First();
                    msgList.Add(thread.thread, thread.refcount);
                }

                var finalList = msgList.OrderByDescending(o => o.Key.UniqueId).Skip(paging.skip).Take(page_size).ToList();

                foreach (var info in finalList)
                {
                    var thread = new GmailThread()
                    {
                        threadID = info.Key.GMailThreadId.Value,
                        uniqueId = info.Key.UniqueId.Id,
                        snippet = info.Key.PreviewText,
                        subject = info.Key.Envelope.Subject,
                        date = info.Key.Envelope.Date,
                        referencesCount = info.Value,
                        to = info.Key.Envelope.To.Mailboxes.Select(a => new Receiver { address = a.Address, name = a.Name }).ToList(),
                        from = info.Key.Envelope.From.Mailboxes.Select(a => new Sender { address = a.Address, name = a.Name }).ToList(),
                        flag = info.Key.Flags.ToString(),
                        hasAttachment = info.Key.Attachments.Any(),
                    };
                    gmailThreads.Add(thread);
                }  
                imap.Disconnect(true);
            }

            return gmailThreads;
        }
        public List<IMailFolder> CollectAllFolders(IMailFolder mailFolders, List<IMailFolder> folderList)
        {
            foreach (var folder in mailFolders.GetSubfolders(false))
            {
                if (folder.Attributes.HasFlag(FolderAttributes.HasChildren))
                {
                    CollectAllFolders(folder, folderList);
                }

                if (folder.Exists)
                {
                    folderList.Add(folder);
                }
            }
            return folderList;
        }
        public void ShowThread(int level, Limilabs.Client.IMAP.MessageThread thread, List<Limilabs.Client.IMAP.Envelope> envelopes)
        {
            string indent = new string(' ', level * 4);
            string subject = envelopes.Find(x => x.UID == thread.UID).Subject;
            Console.WriteLine("{0}{1}: {2}", indent, thread.UID, subject);

            foreach (Limilabs.Client.IMAP.MessageThread child in thread.Children)
            {
                ShowThread(level + 1, child, envelopes);
            }
        }
        //public List<Thread> GetThreads()
        //{
        //    List<Thread> threadList = new List<Thread>();
        //    //using (Imap imap = new Imap())
        //    //{
        //    //    imap.ConnectSSL("imap.mail.ru");

        //    //    ThreadMethod method = ThreadMethod.ChooseBest(imap.SupportedThreadMethods());
        //    //    if (method == null)
        //    //        throw new Exception("This server does not support any threading algorithm.");

        //    //    imap.SelectInbox();

        //    //    List<Limilabs.Client.IMAP.MessageThread> threads = imap.Thread(method).Where(Expression.All());
        //    //    List<Limilabs.Client.IMAP.Envelope> envelopes = imap.GetEnvelopeByUID(
        //    //        imap.Search().Where(Expression.All()));

        //    //    foreach (Limilabs.Client.IMAP.MessageThread thread in threads)
        //    //    {
        //    //        ShowThread(0, thread, envelopes);
        //    //    }
        //    //    imap.Close();
               
        //    //}

        //    using (var imap = new ImapClient())
        //    {
        //        imap.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
        //        imap.Authenticate("testgml922@gmail.com", "Atestg123");
        //        imap.Inbox.Open(FolderAccess.ReadOnly);
        //        var aa = imap.ThreadingAlgorithms;
        //        var messages = imap.Inbox.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.Envelope | MessageSummaryItems.References);
        //        var threads = MessageThreader.Thread(messages, ThreadingAlgorithm.References);

        //        foreach (var rfr in messages)
        //        {
        //            var aaa = rfr.References;
        //            if (rfr.GMailThreadId.HasValue)
        //            {
        //                var _uids = imap.Inbox.Search(SearchQuery.GMailThreadId(rfr.GMailThreadId.Value));
        //            }

        //        }

        //        foreach (var item in messages)
        //        {
        //            var thread = new Thread()
        //            {
        //                threadId = item.ThreadId,
        //                //snippet
        //                subject = item.Envelope.Subject,
        //                date = item.Envelope.Date,
        //                messagesIDs = item.References,
        //                receivers = item.Envelope.To.ToString(),
        //                senders = item.Envelope.From.ToString(),
        //                labels = item.GMailLabels,
        //                hasAttachment = item.Attachments.Any()

        //            };
        //            threadList.Add(thread);
        //        }

        //        imap.Disconnect(true);
        //    }

        //    return threadList;
        //}
        public static void DownloadMessages()
        {
            using (var client = new ImapClient())
            {
                client.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);

                client.Authenticate("username", "password");

                client.Inbox.Open(FolderAccess.ReadOnly);

                var uids = client.Inbox.Search(SearchQuery.All);

                foreach (var uid in uids)
                {
                    var message = client.Inbox.GetMessage(uid);

                    // write the message to a file
                    message.WriteTo(string.Format("{0}.eml", uid));
                }

                client.Disconnect(true);
            }
        }
        public static void DownloadBodyParts()
        {
            using (var client = new ImapClient())
            {
                client.Connect("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);

                client.Authenticate("username", "password");

                client.Inbox.Open(FolderAccess.ReadOnly);

                // search for messages where the Subject header contains either "MimeKit" or "MailKit"
                var query = SearchQuery.SubjectContains("MimeKit").Or(SearchQuery.SubjectContains("MailKit"));
                var uids = client.Inbox.Search(query);

                // fetch summary information for the search results (we will want the UID and the BODYSTRUCTURE
                // of each message so that we can extract the text body and the attachments)
                var items = client.Inbox.Fetch(uids, MessageSummaryItems.UniqueId | MessageSummaryItems.BodyStructure);
                var directory = "path";
                var baseDirectory = "Todo";
                foreach (var item in items)
                {
                    // determine a directory to save stuff in
                    directory = Path.Combine(baseDirectory, item.UniqueId.ToString());

                    // create the directory
                    Directory.CreateDirectory(directory);

                    // IMessageSummary.TextBody is a convenience property that finds the 'text/plain' body part for us
                    var bodyPart = item.TextBody;

                    // download the 'text/plain' body part
                    var body = (TextPart)client.Inbox.GetBodyPart(item.UniqueId, bodyPart);

                    // TextPart.Text is a convenience property that decodes the content and converts the result to
                    // a string for us
                    var text = body.Text;

                    File.WriteAllText(Path.Combine(directory, "body.txt"), text);

                    // now iterate over all of the attachments and save them to disk
                    foreach (var attachment in item.Attachments)
                    {
                        // download the attachment just like we did with the body
                        var entity = client.Inbox.GetBodyPart(item.UniqueId, attachment);

                        // attachments can be either message/rfc822 parts or regular MIME parts
                        if (entity is MessagePart)
                        {
                            var rfc822 = (MessagePart)entity;

                            var path = Path.Combine(directory, attachment.PartSpecifier + ".eml");

                            rfc822.Message.WriteTo(path);
                        }
                        else
                        {
                            var part = (MimePart)entity;

                            // note: it's possible for this to be null, but most will specify a filename
                            var fileName = part.FileName;

                            var path = Path.Combine(directory, fileName);

                            // decode and save the content to a file
                            using (var stream = File.Create(path))
                                part.Content.DecodeTo(stream);
                        }
                    }
                }

                client.Disconnect(true);
            }
        }
        //using (var client = new ImapClient(new ProtocolLogger(Console.OpenStandardOutput())))
        //{
        //    var credentials = new NetworkCredential(username, Password);
        //    var uri = new Uri("imaps://imap.gmail.com");

        //    using (var cancel = new CancellationTokenSource())
        //    {
        //        client.Connect(uri, cancel.Token);

        //        // Note: Since we don't have an OAuth2 token, disable
        //        // the XOAUTH2 authentication mechanism.
        //        client.AuthenticationMechanisms.Remove("XOAUTH2");

        //        client.Authenticate(credentials, cancel.Token);

        //        // Get the first personal namespace and list the toplevel folders under it.
        //        var personal = client.GetFolder(client.PersonalNamespaces[0]);
        //        foreach (var folder in personal.GetSubfolders(false, cancel.Token))
        //            Console.WriteLine("[folder] {0}", folder.Name);

        //        client.Disconnect(true, cancel.Token);
        //    }
        //}
    }
}
