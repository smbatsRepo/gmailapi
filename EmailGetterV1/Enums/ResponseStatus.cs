﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailGetterV1.Enums
{
    public enum ResponseStatus
    {
        Success = 1,
        Failed = 2,
        Deleted = 3,
    }
}
