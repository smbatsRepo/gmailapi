﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailGetterV1.Models
{
    public class Pagination
    {
        public int pageSize { get; set; }
        public int page { get; set; }
        public int totalItems { get; set; }
        public int TotalPages => (int)Math.Ceiling(decimal.Divide(totalItems, pageSize)) - 1;
    }
   
    public class Paging
    {
        public int skip { get; set; }
        public int take { get; set; }

        public Paging(int page, int page_size)
        {
            skip = page * page_size;
            take = (page * page_size) + (page_size - 1);
        }
    }
    public class PaginationInfo
    {
        public int CurrentPage { get; set; }

        public bool IsPagingRequired { get; set; }

        public int PagerLength { get; set; }

        public int PageSize { get; set; }

        public string PageSizeDisplay
        {
            get
            {
                return PageSize == 1 ? "All" : PageSize.ToString();
            }
        }

        public int[] PageSizeItems
        {
            get
            {
                return new[] { 25, 50, 75, 100, 1 };
            }
        }

        public int TotalPages { get; set; }

        public int TotalRecords { get; set; }
    }
}
