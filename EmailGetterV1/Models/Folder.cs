﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailGetterV1.Models
{
    public class Folder
    {
        public string name { get; set; }
        public int messagesTotal { get; set; }
        public int messagesUnread { get; set; }
        public int threadsTotal { get; set; }
        public int threadsUnread { get; set; }
    }
}
