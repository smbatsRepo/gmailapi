﻿using MailKit;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailGetterV1.Models
{
    public class GmailMessage
    {
        public GmailMessage()
        {
            to = new List<Receiver>();
            from = new List<Sender>();
            references = new List<Message>();
        }
        public ulong threadID { get; set; }
        public uint uniqueId { get; set; }
        public string snippet { get; set; }
        public string subject { get; set; }
        public DateTimeOffset? date { get; set; }
        public List<Message> references { get; set; }
        public List<Sender> from { get; set; }
        public List<Receiver> to { get; set; }
        public string flag { get; set; }
        //public bool isReply { get; set; }
        //public MessageIdList referencedMessageIds { get; set; }
        //public IEnumerable<BodyPartBasic> attachments { get; set; }
        public bool hasAttachment { get; set; }
    }

    public class Message
    {
        public string body { get; set; }
        public uint uniqueId { get; set; }
        public DateTimeOffset? date { get; set; }
        public List<Sender> from { get; set; }
        public List<Receiver> to { get; set; }
        public List<AttachmentInfo> Attachments { get; set; }
    }

    public class AttachmentInfo
    {
        public string messageId { get; set; }
        public string fileName { get; set; }
        public long? fileSize { get; set; }
        public string mediaSubtype { get; set; }
        public string type { get; set; }
        public string content { get; set; }

    }

}
