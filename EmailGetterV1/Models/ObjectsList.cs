﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailGetterV1.Models
{
    public class ObjectsList<T>
    {
        public ObjectsList()
        {
            Items = new List<T>();
        }
        public Pagination pagination { get; set; }

        public List<T> Items { get; set; }
    }
}
