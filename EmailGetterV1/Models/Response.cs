﻿using EmailGetterV1.Enums;
using EmailGetterV1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailGetterV1.Helpers
{
    public class Response
    {
        public ResponseStatus status { get; set; }
        public string Message { get; set; }
        public bool succsess { get; set; }
        public object Data { get; set; }
        public Pagination pagination { get; set; }

    }
}
