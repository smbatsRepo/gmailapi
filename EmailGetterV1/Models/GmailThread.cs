﻿using MailKit;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailGetterV1.Models
{
    public class GmailThread
    {
        public GmailThread()
        {
            to = new List<Receiver>();
            from = new List<Sender>();
        }
        public ulong threadID { get; set; }
        public uint uniqueId { get; set; }
        public string snippet { get; set; }
        public string subject { get; set; }
        public DateTimeOffset? date { get; set; }
        public int referencesCount { get; set; }
        public List<Sender> from { get; set; }
        public List<Receiver> to { get; set; }
        public string flag { get; set; }
        //public bool isReply { get; set; }
        //public MessageIdList referencedMessageIds { get; set; }
        //public IEnumerable<BodyPartBasic> attachments { get; set; }
        public bool hasAttachment { get; set; }
    }
    public class Receiver
    {
        public string name { get; set; }
        public string address { get; set; }

    }
    public class Sender
    {
        public string name { get; set; }
        public string address { get; set; }

    }
}
